import EventEmitter from "eventemitter3";

export default class Application extends EventEmitter {
  static get events() {
    return {
      READY: "ready",
    };
  }

  constructor() {
    super();
    this.emojis = [];
    this.banana = "🍌";
    this.emit(Application.events.READY);
  }
  setEmojis(emojis) {
    this.emojis = emojis;
    let emojiDiv = document.getElementById("emojis");
    emojiDiv.innerHTML = "";
  }

  addBananas() {
    let monkeyBanana = this.emojis.map((monkey) => monkey + this.banana);
    monkeyBanana.forEach((emoji) => {
      let div = document.getElementById("emojis");
      let paragraph = document.createElement("p");
      paragraph.textContent = emoji;
      div.appendChild(paragraph);
      return monkeyBanana;
    });
  }
}
